# About

This is a general programming library used to build datascience projects. Example dataflow is assumed to be:

```
ingest_input \
	| feature extraction \
	| vectorspace \
	| (classifier | postprocessing | publish, 
	   documentspace | visualization | publish)
```

# Requirements

* Fetch and install `libcudnn7_7.6.5.32-1+cuda10.1_amd64.deb` from nvidia.
* `apt-get install cmake swig python3-dev`

# Installation

python3 -m pip install .

# TODO

* Add Dockerfile to define the running environment (ubuntu18.04 + lambda?)


# Design constraints

* Scalable
* Realtime
* Input agnostic
* Maybe asynchronous?
* Sklearn make_pipeline, make_union
* Wrappers to intuitively use Tensorflow, Keras, PyTorch, ...
* Works with Kafka
* Works like bash commands:
  * `make_pipeline(FeatureExtractor(), DocumentSpace(), Visualize())`
  * `cat data.json|FeatureExtractor|DocumentSpace|Visualize`
* Each step should add data to existing object
* Each step should be serializeable and human printable
* What if there is alternative interpretations? n-best?

# API

```python
bs = Library.visualize.BokehScatterplot()

df = [{text, datetime, author, title, category, thread, thread-text}]
feature_pipeline = make_pipeline(ColumnSelector('text'), Library.FeatureExtractor.Text(lang='fi'))
bs.plot(df, feature_pipeline)

df = [audiochunks]
bs.plot(df, Library.FeatureExtractor.Audio(timestamps=True))

df = [speech]
bs.plot(df, Library.FeatureExtractor.Speech(timestamps=True))

df = [SensorEvents]
bs.plot(df, Library.FeatureExtractor.SensorEvent())
```
