FROM ubuntu:18.04

# This image contains machine learning and datascience tools for
# speech, audio, video and general data pipeline creation. Use this
# to develop the Library and services around it.

WORKDIR /root/

# Add libcuda dummy dependency
ADD control .
RUN apt-get update && \
	apt-get install --yes equivs && \
	equivs-build control && \
	dpkg -i libcuda1-dummy_10.0_all.deb && \
	rm control libcuda1-dummy_10.0_all.deb && \
	apt-get remove --yes --purge --autoremove equivs && \
	rm -rf /var/lib/apt/lists/*

# Setup Lambda repository
ADD lambda.gpg .
RUN apt-get update && \
	apt-get install --yes gnupg && \
	apt-key add lambda.gpg && \
	rm lambda.gpg && \
	echo "deb http://archive.lambdalabs.com/ubuntu bionic main" > /etc/apt/sources.list.d/lambda.list && \
	echo "Acquire::http::Pipeline-Depth 0;" > /etc/apt/apt.conf.d/99fixbadproxy && \
  echo "Acquire::http::No-Cache true;"   >> /etc/apt/apt.conf.d/99fixbadproxy && \
  echo "Acquire::BrokenProxy    true;"   >> /etc/apt/apt.conf.d/99fixbadproxy && \
	echo "Package: *" > /etc/apt/preferences.d/lambda && \
	echo "Pin: origin archive.lambdalabs.com" >> /etc/apt/preferences.d/lambda && \
	echo "Pin-Priority: 1001" >> /etc/apt/preferences.d/lambda && \
	echo "cudnn cudnn/license_preseed select ACCEPT" | debconf-set-selections && \
	apt-get update && \
	DEBIAN_FRONTEND=noninteractive \
		apt-get install --no-install-recommends --yes lambda-stack-cuda lambda-server && \
	rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install --yes python3-dev

#RUN useradd -d /home/ubuntu -ms /bin/bash -g root -G sudo -p ubuntu ubuntu
#ENV NAME ubuntu
#ENV HOME /home/ubuntu
# USER ubuntu

#WORKDIR "/home/ubuntu"

ENV LD_LIBRARY_PATH /usr/lib/x86_64-linux-gnu

# Setup for nvidia-docker
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=10.0"

ADD . .
RUN pip3 install -U pip
RUN apt-get update && apt-get install -y swig
RUN pip3 install variKN/
RUN pip3 install wrapt==1.11.1 --upgrade --ignore-installed
RUN pip3 install .
RUN pip3 install .[tests]
CMD pytest -s Library
