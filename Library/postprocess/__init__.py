import sys

import numpy as np
import logging

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.cluster import DBSCAN
from sklearn.mixture import GaussianMixture
from sklearn.neighbors import LocalOutlierFactor, NearestNeighbors
from collections import Counter

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# logger.addHandler(logging.StreamHandler())


def error_counts(y_prob, labels):
    """
    Count errors in the predictions
    :param y_prob: predictions
    :param labels: true results
    :return: decision boundaries, true_false error counts, false_true error counts
    """
    prob_idx = y_prob.argsort()
    false_negative_counts = sum(1 - labels) - np.add.accumulate(1 - labels[prob_idx])
    false_positive_counts = np.add.accumulate(labels[prob_idx])
    return y_prob[prob_idx], false_positive_counts, false_negative_counts


class Transformation(BaseEstimator, TransformerMixin):
    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X)


class DBSCAN_GMM(Transformation):
    def __init__(self, k=1, tau=500, eps=0.1, **kwargs):
        self.dbscan = None
        self.gmm = None
        self.eps = eps
        self.kwargs = kwargs
        self.k = k
        self.tau = tau

    def fit(self, X, y=None):
        min_samples = max(5, int(np.sqrt(len(X)) / 5))
        self.dbscan = DBSCAN(eps=self.eps, min_samples=min_samples, **self.kwargs)
        self.dbscan.fit(X)
        logger.info("DBSCAN found %d clusters", len(Counter(self.dbscan.labels_)))
        # Lets be reasonable with the number of gmm components
        n_gmm_components = min(50, self.k * len(Counter(self.dbscan.labels_)))
        logger.info("Creating gmm with %d components", n_gmm_components)
        self.gmm = GaussianMixture(n_components=n_gmm_components)
        self.gmm.fit(X)
        return self

    def _gmm_cov_norm(self):
        return np.array([np.linalg.norm(c) for c in self.gmm.covariances_])

    def transform(self, X, y=None):
        return np.exp(self.gmm._estimate_log_prob() + np.log(self._gmm_cov_norm())).sum(axis=1)


class AnomalyDetection(Transformation):
    def __init__(self, n=5):
        self.lof = LocalOutlierFactor(n_neighbors=n, novelty=True)

    def fit(self, X, y=None):
        logger.info("Fitting novelty flagger")
        self.lof.fit(X)
        return self

    def transform(self, X, y=None):
        logger.info("Flagging novelties")
        T = np.arctan(self.lof.score_samples(X).reshape(-1, 1))
        logger.info("%s produces shape %s", self.__class__, T.shape)
        return T


class NeighbourDistances(Transformation):
    def __init__(self, neighbour_sizes=[10], n_jobs=1, metric="cosine"):
        self.neighbours = [NearestNeighbors(i, n_jobs=n_jobs, metric=metric) for i in neighbour_sizes]
        self.n_jobs = n_jobs

    def fit(self, X, y=None):
        logger.info("Calculating neighbour index")
        for neigh in self.neighbours:
            neigh.fit(X)
            sys.stderr.write(".")
        sys.stderr.write(" done\n")
        return self

    def transform(self, X, y=None):
        logger.info("Calculating neighbours")
        neighbours_dists = []
        for neigh in self.neighbours:
            neighbours_dists.append(neigh.kneighbors(X))
            sys.stderr.write(".")
        sys.stderr.write(" done\n")
        D, _ = zip(*neighbours_dists)
        T = np.hstack([d.mean(axis=1).reshape(-1, 1) for d in D])
        logger.info("%s produces shape %s", self.__class__, T.shape)
        return T


class PercentageModel:
    """
    We usually want to minimize our errors or produce errors which have a 50/50 chance
    of belonging to either class. This class currently works only for binary classification
    tasks.
    """

    def __init__(self, mode="balanced"):
        self.mode = mode
        self.decision_boundary = 0.5

    def fit(self, X, y):
        probs, false_neg, false_pos = error_counts(X[:, 1], y)
        if self.mode == "min_errors":
            errors = false_neg + false_pos
            self.decision_boundary = probs[errors[::-1].argmin()]
            logger.info("Minimum errors: %d", errors.min())
        elif self.mode == "balanced":
            errs = np.abs(false_neg - false_pos)
            errordiffmini = errs.argmin()
            self.decision_boundary = probs[errordiffmini]
            logger.info("Balanced errors: (%d, %d)", false_neg[errordiffmini], false_pos[errordiffmini])
        else:
            raise NotImplementedError("Unknown decision method requested")
        logger.info("Decition boudary: %.3f", self.decision_boundary)
        return self

    def _power_tranform(self, X):
        return np.power(X, np.log2(0.5) / np.log2(self.decision_boundary))

    def transform(self, X, y=None):
        tmpX = self._power_tranform(X[:, 1])
        return np.vstack([1 - tmpX, tmpX]).T

    def predict(self, X, y=None):
        return self.transform(X)

    def fit_transform(self, X, y):
        """Fit and transform classifier results so that we get a nice decision boundary.

        The fit_transform-function tests every function of the class.

        >>> X1 = np.arange(0,1,0.1)
        >>> X = np.vstack([1 - X1, X1]).T
        >>> y = np.array([0,0,0,1,0,1,1,1,1,1])
        >>> Xp = PercentageModel('min_errors').fit_transform(X,y)[:,1].round(5)
        >>> round(Xp[5], 1)
        0.5
        >>> sum(Xp < 0.5)
        5
        >>> sum(Xp >= 0.5)
        5
        >>> Xp2 = PercentageModel('balanced').fit_transform(X,y)[:,1].round(5)
        >>> round(Xp2[3], 1)
        0.5
        >>> sum(Xp2 < 0.5)
        3
        >>> sum(Xp2 >= 0.5)
        7

        # >>> 1*(Xp2 > 0.5), y
        """
        return self.fit(X, y).transform(X)
