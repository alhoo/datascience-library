from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils import check_X_y, check_array

import tensorflow as tf

from keras.models import load_model

from keras import models

from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Embedding
from keras.layers import SeparableConv1D
from keras.layers import MaxPooling1D
from keras.layers import GlobalAveragePooling1D

from keras.optimizers import Adam

import logging
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


def _get_last_layer_units_and_activation(num_classes):
    """Gets the # units and activation function for the last network layer.

    # Arguments
        num_classes: int, number of classes.

    # Returns
        units, activation values.
    """
    if num_classes == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = num_classes
    return units, activation


def sepcnn_model(
    blocks,
    filters,
    kernel_size,
    embedding_dim,
    dropout_rate,
    pool_size,
    input_shape,
    num_classes,
    num_features,
    use_pretrained_embedding=False,
    is_embedding_trainable=False,
    embedding_matrix=None,
):
    """Creates an instance of a separable CNN model.

    # Arguments
        blocks: int, number of pairs of sepCNN and pooling blocks in the model.
        filters: int, output dimension of the layers.
        kernel_size: int, length of the convolution window.
        embedding_dim: int, dimension of the embedding vectors.
        dropout_rate: float, percentage of input to drop at Dropout layers.
        pool_size: int, factor by which to downscale input at MaxPooling layer.
        input_shape: tuple, shape of input to the model.
        num_classes: int, number of output classes.
        num_features: int, number of words (embedding input dimension).
        use_pretrained_embedding: bool, true if pre-trained embedding is on.
        is_embedding_trainable: bool, true if embedding layer is trainable.
        embedding_matrix: dict, dictionary with embedding coefficients.

    # Returns
        A sepCNN model instance.
    """
    op_units, op_activation = _get_last_layer_units_and_activation(num_classes)
    model = models.Sequential()

    # Add embedding layer. If pre-trained embedding is used add weights to the
    # embeddings layer and set trainable to input is_embedding_trainable flag.
    if use_pretrained_embedding:
        model.add(
            Embedding(
                input_dim=num_features,
                output_dim=embedding_dim,
                input_length=input_shape[0],
                weights=[embedding_matrix],
                trainable=is_embedding_trainable,
            )
        )
    else:
        model.add(Embedding(input_dim=num_features, output_dim=embedding_dim, input_length=input_shape[0]))

    for _ in range(blocks - 1):
        model.add(Dropout(rate=dropout_rate))
        model.add(
            SeparableConv1D(
                filters=filters,
                kernel_size=kernel_size,
                activation="relu",
                bias_initializer="random_uniform",
                depthwise_initializer="random_uniform",
                padding="same",
            )
        )
        model.add(
            SeparableConv1D(
                filters=filters,
                kernel_size=kernel_size,
                activation="relu",
                bias_initializer="random_uniform",
                depthwise_initializer="random_uniform",
                padding="same",
            )
        )
        model.add(MaxPooling1D(pool_size=pool_size))

    model.add(
        SeparableConv1D(
            filters=filters * 2,
            kernel_size=kernel_size,
            activation="relu",
            bias_initializer="random_uniform",
            depthwise_initializer="random_uniform",
            padding="same",
        )
    )
    model.add(
        SeparableConv1D(
            filters=filters * 2,
            kernel_size=kernel_size,
            activation="relu",
            bias_initializer="random_uniform",
            depthwise_initializer="random_uniform",
            padding="same",
        )
    )
    model.add(GlobalAveragePooling1D())
    model.add(Dropout(rate=dropout_rate))
    model.add(Dense(op_units, activation=op_activation))
    return model


def sequence_vectorize(a, b, num_words=50000, max_length=500, stemmer=None):  # noqa
    return NotImplementedError()


def train_sequence_model(
    data,
    blocks=2,
    filters=64,
    embedding_dim=160,
    kernel_size=3,
    pool_size=3,
    learning_rate=1e-3,
    epochs=1000,
    batch_size=128,
    dropout_rate=0.2,
    stemmer=None,
):
    (val_texts, val_labels), (train_texts, train_labels) = data
    num_classes = len(set(list(train_labels)))
    x_train, x_val, vocab = sequence_vectorize(train_texts, val_texts, num_words=50000, max_length=500, stemmer=stemmer)
    model = sepcnn_model(
        blocks=blocks,
        filters=filters,
        kernel_size=kernel_size,
        embedding_dim=embedding_dim,
        dropout_rate=dropout_rate,
        pool_size=pool_size,
        input_shape=x_train.shape[1:],
        num_classes=num_classes,
        num_features=len(vocab),
        use_pretrained_embedding=False,
        is_embedding_trainable=False,
        embedding_matrix=None,
    )
    if num_classes == 2:
        loss = "binary_crossentropy"
    else:
        loss = "sparse_categorical_crossentropy"
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer, loss=loss, metrics=["acc"])

    callbacks = [tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=2)]
    # restore_best_weights=True)]

    model.fit(
        x_train,
        train_labels == "accepted",
        epochs=epochs,
        callbacks=callbacks,
        validation_data=(x_val, val_labels == "accepted"),
        verbose=2,  # Logs once per epoch.
        batch_size=batch_size,
    )
    return model, vocab


class KerasTransformer(BaseEstimator, ClassifierMixin):
    def __init__(self, model=None, filename=None):
        self.filename = "keras_transformer_model.h5"
        self.model = model
        if filename is not None:
            self.filename = filename
            if model is None:
                self.model = load_model(self.filename)
        if self.model is None:
            raise ("No model specified! Specify existing model filename or build the model for me.")

    def fit(self, X, y=None, **kwargs):
        X, y = check_X_y(X, y)
        self.model.fit(X, y, **kwargs)
        return self

    def transform(self, X, y=None):
        X, y = check_array(X)
        return self.model.predict(X)


class SepCNNTransformer(KerasTransformer):
    def __init__(
        self,
        blocks=2,
        filters=64,
        embedding_dim=160,
        kernel_size=3,
        pool_size=3,
        learning_rate=1e-3,
        epochs=1000,
        batch_size=128,
        dropout_rate=0.2,
        stemmer=None,
        verbose=0,
    ):
        self.blocks = blocks
        self.filters = filters
        self.embedding_dim = embedding_dim
        self.kernel_size = kernel_size
        self.pool_size = pool_size
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.batch_size = batch_size
        self.dropout_rate = dropout_rate
        self.stemmer = stemmer
        self.verbose = verbose

    def _more_tags(self):
        return {"X_types": "categorical"}

    def init_model(self, X, y):
        num_features = X.max() + 1
        num_classes = len(set(list(y)))
        self.model = sepcnn_model(
            blocks=self.blocks,
            filters=self.filters,
            kernel_size=self.kernel_size,
            embedding_dim=self.embedding_dim,
            dropout_rate=self.dropout_rate,
            pool_size=self.pool_size,
            input_shape=X.shape[1:],
            num_classes=num_classes,
            num_features=num_features,
            use_pretrained_embedding=False,
            is_embedding_trainable=False,
            embedding_matrix=None,
        )
        if num_classes == 2:
            loss = "binary_crossentropy"
        else:
            loss = "sparse_categorical_crossentropy"
        # optimizer = tf.keras.optimizers.Adam(lr=self.learning_rate)
        optimizer = "adam"
        self.model.compile(optimizer=optimizer, loss=loss, metrics=["acc"])

    def fit(self, X, y, validation_split=0.01, **kwargs):
        self.init_model(X, y)
        logger.info("Using early stopping with patience 2")
        callbacks = [tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=2)]
        X, y = check_X_y(X, y)
        self.model.fit(
            X,
            y,
            epochs=self.epochs,
            callbacks=callbacks,
            verbose=self.verbose,
            batch_size=self.batch_size,
            validation_split=validation_split,
            **kwargs
        )
        return self

    def transform(self, X, y=None):
        """Transform data using SepCNN

        >>> from sklearn.utils.estimator_checks import check_estimator
        >>> from Library.text import *
        >>> from Library.utils import *
        >>> from nltk.stem.snowball import SnowballStemmer
        >>> from sklearn.pipeline import make_pipeline
        >>> import pandas as pd
        >>> check_estimator(SepCNNTransformer)
        >>> df = pd.DataFrame({'content': ['Hello', 'Hi'], 'result': ['accepted', 'improper']})
        >>> cs = ColumnSelector('content')
        >>> tok = Tokenizer(SnowballStemmer('portuguese'))
        >>> seqvec = SequenceVectorizer(max_length=500, num_words=40000)
        >>> try:
        ...   sepcnn = SepCNNTransformer()
        ...   tokpipe = make_pipeline(cs, tok, seqvec)
        ...   fts = tokpipe.fit_transform(df).shape
        ...   nnpipe = make_pipeline(cs, tok, seqvec, sepcnn)
        ...   tfs2 = nnpipe.fit_transform(df, ColumnSelector('result').transform(df) == 'accepted').shape
        ... except:
        ...   pass
        """
        X = check_array(X)
        return self.model.predict_proba(X)

    def predict_proba(self, X):
        return self.model.predict_proba(X)

    def predict(self, X):
        return self.model.predict_proba(X)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
