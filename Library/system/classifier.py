import numpy as np

from scipy import sparse

# from sklearn import metrics
from sklearn.feature_selection import SelectPercentile, f_classif, SelectKBest, chi2
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer
from sklearn.pipeline import make_pipeline

from nltk.stem.snowball import SnowballStemmer

from Library.text import ListCountVectorizer, Tokenizer
from Library.postprocess import PercentageModel
from Library.utils import ToTransformer

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Classifier:
    def __init__(
        self,
        feature_extraction_pipeline,
        classifier_pipeline,
        postprocess_pipeline,
        k_features=100000,
        select_method="chi2",
        **kwargs
    ):
        fa = chi2
        if select_method == "f_classif":
            fa = f_classif
        if k_features < 1:
            self.selector = SelectPercentile(fa, k=k_features)
        else:
            self.selector = SelectKBest(fa, k=k_features)

        if feature_extraction_pipeline is None:
            self.feature_extraction_pipeline = self._build_default_preprocessing()
        else:
            self.feature_extraction_pipeline = feature_extraction_pipeline
        if classifier_pipeline is None:
            self.classifier_pipeline = self._default_classifier()
        else:
            self.classifier_pipeline = classifier_pipeline
        if postprocess_pipeline is not None:
            self.postprocess_pipeline = postprocess_pipeline
        else:
            self.postprocess_pipeline = PercentageModel()
        self.cls = make_pipeline(self.feature_extraction_pipeline, self.classifier_pipeline)
        self.pl = make_pipeline(ToTransformer(self.cls), self.postprocess_pipeline)

    def fit(self, X, y):
        idxs = np.arange(len(X))
        np.random.shuffle(idxs)
        try:
            idxs = X.index[idxs]
        except:  # noqa
            pass
        testlim = max(1000, int(len(X) * 0.05))
        self.cls.fit(X[idxs[testlim:]], y[idxs[testlim:]])
        y_pred = self.cls.predict_proba(X[idxs[:testlim]])
        self.postprocess_pipeline.fit(y_pred, np.array(y[:testlim]))

    def transform(self, X, y=None):
        return self.pl.transform(X)

    def predict_proba(self, X, y=None):
        return self.transform(X, y)

    def predict(self, X, y=None):
        p = self.transform(X, y)
        return p[:, 0] > p[:, 1]

    # def evaluate(self, X, y):
    #     y_pred = self.predict_proba(X)[:, 1]
    #     return y_pred, metrics.roc_auc_score(y, y_pred), confusion(y, (y_pred > 0.5))


class union_make:
    def __init__(self, *args):
        self.parts = args

    def fit(self, X, y=None):
        for p in self.parts:
            p.fit(X, y)
        return self

    def transform(self, X, y=None):
        Xs = [p.transform(X) for p in self.parts]
        if any(sparse.issparse(f) for f in Xs):
            Xs = sparse.hstack(Xs).tocsr()
        else:
            Xs = np.hstack(Xs)
        return Xs

    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)


def build_text_features(lang="english"):
    from sklearn.preprocessing import Normalizer

    def norm_tfidf(ng_range=(1, 1)):
        return make_pipeline(
            ListCountVectorizer(min_df=2, max_df=0.4, ngram_range=ng_range), TfidfTransformer(), Normalizer()
        )

    features2 = union_make(
        make_pipeline(Tokenizer(SnowballStemmer(lang)), norm_tfidf((1, 2))),
        make_pipeline(Tokenizer(), norm_tfidf((1, 1))),
    )

    text_features = union_make(features2, TfidfVectorizer(min_df=2, max_df=0.4, ngram_range=(4, 4), analyzer="char"))
    return text_features


class TextClassifier(Classifier):
    def __init__(
        self,
        feature_extraction_pipeline=None,
        classifier_pipeline=None,
        postprocess_pipeline=None,
        k_features=100000,
        select_method="f_classif",
        lang="en",
    ):
        """
        Classifier for text data

        :param feature_extraction_pipeline: transformers to fit/apply in the preprocessing step
        :param classifier_pipeline: classifier
        :param postprocess_pipeline: transformers to fit/apply to classifier results
        :param k_features: number of features to use for classification
        :param select_method: feature selection method (chi2/f_classif)
        :param lang: data language (used in building the default feature_extraction pipeline)

        >>> tc = TextClassifier(lang='portuguese')
        """
        self.lang = lang
        super(TextClassifier, self).__init__(
            feature_extraction_pipeline=None,
            classifier_pipeline=None,
            postprocess_pipeline=None,
            k_features=100000,
            select_method="f_classif",
        )

    def _build_default_preprocessing(self):
        return build_text_features(self.lang)

    @staticmethod
    def _default_classifier():
        return LogisticRegression(C=0.5, solver="lbfgs")
