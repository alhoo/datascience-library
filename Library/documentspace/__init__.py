# from parametric_tSNE import Parametric_tSNE
from sklearn.base import BaseEstimator
from sklearn.pipeline import make_pipeline, make_union, Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE, Isomap, MDS
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd

import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s %(name)s %(levelname)s: %(message)s", level=logging.INFO)


def with_fit_transform(M):
    def fit(X, y=None):
        M.fit_transform(X)
        return M

    def transform(X, y=None):
        return M.fit_transform(X)

    setattr(M, "fit", fit)
    setattr(M, "transform", transform)
    return M


def with_fit_transforms(arr):
    for i, M in enumerate(arr):
        if hasattr(M, "fit") and hasattr(M, "transform"):
            continue
        arr[i] = with_fit_transform(M)
    return arr


class DocumentSpace(Pipeline):
    def __init__(self, transformations=[TSNE(), Isomap(), MDS(), PCA(2)]):
        steps = [("dimensions", make_union(*with_fit_transforms(transformations))), ("scaler", StandardScaler())]
        super().__init__(steps=steps)
        self.names = [name for name, _ in self.steps[0][1].transformer_list]

    def fit(self, *args, **kwargs):
        vec = super().fit(*args, **kwargs)
        return self

    def _transform_one(self, transformer, X, y, weight, **fit_params):
        return transformer.transform(X, y)

    def transform(self, *args, **kwargs):
        vec = super().transform(*args, **kwargs)
        columns = pd.MultiIndex.from_product([self.names, ["x", "y"]])
        return pd.DataFrame(vec, columns=columns)

    def fit_transform(self, *args, **kwargs):
        vec = super().fit_transform(*args, **kwargs)
        columns = pd.MultiIndex.from_product([self.names, ["x", "y"]])
        return pd.DataFrame(vec, columns=columns)
