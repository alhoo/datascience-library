import os
import sys
from math import log10, pow
from subprocess import Popen, PIPE
from threading import Thread
from queue import Queue, Empty
import copy
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class DataLogger:
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        nbytes = 0
        pos = 1
        pos_minor = 1
        for x in X:
            nbytes += len(x)
            if nbytes > pos_minor * pow(10, int(log10(nbytes))):
                pos_minor += 1
                sys.stderr.write(".")
            if nbytes >= pos:
                pos = pow(10, int(log10(nbytes)) + 1)
                sys.stderr.write("\n%d: " % nbytes)
                pos_minor = 1
            yield x
        sys.stderr.write("\n")


class NonBlockingStreamReader:
    def __init__(self, stream, queue_size=0):
        """
        stream: the stream to read from.
                Usually a process' stdout or stderr.
        """

        self._s = stream
        self._q = Queue(queue_size)

        def _populateQueue(stream, queue):
            """
            Collect lines from 'stream' and put them in 'queue'.
            """
            while True:
                line = stream.read(1024)
                if line:
                    queue.put(line)
                else:
                    # queue.put(Empty)
                    logger.info("Queue empty. Ending subprocess!")
                    break

        self._t = Thread(target=_populateQueue, args=(self._s, self._q))
        self._t.daemon = True
        self._t.start()

    def __iter__(self):
        return self

    def __next__(self):
        return self.readline()

    def readline(self, timeout=0.1):
        try:
            return self._q.get(block=timeout is not None, timeout=timeout)
        except Empty:
            return None


def cmd_gen(cmd, data=None, fds=[], env=None, queue_size=1024):
    """Run unix commoandline tools as generators"""
    if not isinstance(cmd, list):
        cmd = cmd.split()
    if data is None:
        p1 = Popen(cmd, stdout=PIPE, pass_fds=fds, env=env)
        nbsr = NonBlockingStreamReader(p1.stdout, queue_size)
        logger.info("Reading results from process (%s)...", cmd[0])
        for output in nbsr:
            if output is None:
                break
            yield output
        logger.info("Reading rest of results (%s)...", cmd[0])
        p1.wait()
        while True:
            try:
                output = nbsr.readline(0.1)
                if output is None:
                    break
                yield output
            except Empty:
                break
        logger.info("Done (%s)", cmd[0])
        return
    p1 = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=PIPE, pass_fds=fds, env=env)
    nbsr = NonBlockingStreamReader(p1.stdout, queue_size=0)
    logger.info("Sending data to process (%s)...", cmd[0])
    for chunk in data:
        logger.debug("Sending %d bytes to process (%s)...", len(chunk), cmd[0])
        try:
            p1.stdin.write(chunk)
        except:  # noqa
            pass
        logger.debug("Reading results from process (%s)", cmd[0])
        for output in nbsr:
            if output is None:
                break
            logger.debug("Process produced %d bytes (%s)", len(output), cmd[0])
            yield output

    logger.info("Reading results from process (%s)...", cmd[0])
    for output in nbsr:
        if output is None:
            break
        yield output

    logger.info("Reading rest of results (%s)...", cmd[0])
    p1.stdin.close()
    p1.wait()
    err = p1.stderr.read()
    if err:
        logger.info(err)
    while True:
        try:
            output = nbsr.readline(0.1)
            if output is None:
                break
            yield output
        except Empty:
            break
    logger.info("Done (%s)", cmd[0])


def lchain(gen, maxlen=100 * 1024 * 1024):
    """
    Rechain input to even sized pieces

    >>> igen = ["1234567890", "1234567", "890"]
    >>> list(lchain(igen, 5))
    ['12345', '67890', '12345', '67890']

    :param gen: input generator
    :param maxlen: maximum length of a piece
    :return: generator that produces maxlen pieces
    """
    assert isinstance(maxlen, int)
    part = None
    for c in gen:
        if part is None:
            part = c
        else:
            part += c
        while len(part) > maxlen:
            yield part[:maxlen]
            part = part[maxlen:]
    yield part


def sox_dec_gen(data, datatype="mp3"):
    cmd = "sox -t {} - -t raw -c 1 -b 16 -e signed-integer -r 16000 -".format(datatype)
    return cmd_gen(cmd, data)


def sox_enc_gen(data, datatype="mp3"):
    cmd = "sox -t raw -c 1 -r 16000 -e signed-integer -b 16 - -t {} -".format(datatype)
    return cmd_gen(cmd, data)


def ogg_dec_gen(data):
    return sox_dec_gen(data, "ogg")


def ogg_enc_gen(data):
    return sox_enc_gen(data, "ogg")


def mp3_dec_gen(data):
    return sox_dec_gen(data, "mp3")


def mp3_enc_gen(data):
    return sox_enc_gen(data, "mp3")


class CMDTransformation:
    """Transformation from a unix commandline tool"""

    def __init__(self, cmd, env=None):
        if env is not None:
            new_env = copy.deepcopy(os.environ)
            new_env.update(env)
            self.env = new_env
        else:
            self.env = env
        self.cmd = cmd

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return cmd_gen(self.cmd, X, env=self.env)


class ChainTransformation:
    """Chain input to maxlen sized chunks"""

    def __init__(self, maxlen=100 * 1024 * 1024):
        self.maxlen = maxlen

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return lchain(X, self.maxlen)


class StreamingPipeline:
    """
    Create a streaming pipeline

    >>> inputgen = [b"helloworld"]
    >>> t1 = CMDTransformation("openssl aes-256-cbc -pbkdf2 -pass env:ENCRYPTION_KEY",
    ...                   env={"ENCRYPTION_KEY": "helloworld"})
    >>> t2 = CMDTransformation("openssl aes-256-cbc -pbkdf2 -d -pass env:ENCRYPTION_KEY",
    ...                   env={"ENCRYPTION_KEY": "helloworld"})
    >>> t3 = ChainTransformation(maxlen=5)
    >>> pipe = make_streaming_pipeline(t1, t2, t3)
    >>> list(pipe.transform(inputgen))
    [b'hello', b'world']
    """

    def __init__(self, transformations):
        self.steps = transformations

    def fit(self, X, y=None):
        logger.warning("Not implemented!")
        return self

    def transform(self, X, y=None):
        gen = X
        for t in self.steps:
            gen = t.transform(gen)
        return gen


def make_streaming_pipeline(*transformations):
    return StreamingPipeline(transformations)
