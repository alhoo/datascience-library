from collections import Counter
import numpy as np
from sklearn import metrics


def confusion(y, y_pred):
    return np.array([x[1] for x in sorted(Counter(y * 2 + y_pred.reshape(-1)).items(), reverse=True)]).reshape(2, 2)


def result_summary(test_y, pred_y, decision_boundary=0.5):
    print("Test set (%d):" % (len(test_y)))
    print("  Accuracy: %.3f" % float(sum((1 * (pred_y > decision_boundary).T)[0] == 1 * test_y) / len(test_y)))
    try:
        print("  Auc roc: %.3f" % float(metrics.roc_auc_score(test_y, pred_y)))
    except Exception as ex:
        print("Exception: %r" % ex)
        print(test_y, pred_y)
    print("  Confusion:", "\n             ".join(map(str, confusion(test_y, (pred_y > 0.5)).tolist())))
