import types
from functools import partialmethod

import pandas as pd
import numpy as np
import json

from sklearn.base import BaseEstimator
from sklearn.pipeline import Pipeline, FeatureUnion, make_union
from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import make_pipeline
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# logger.addHandler(logging.StreamHandler())


def flatten_jsonl(line, sep="."):
    y = json.loads(line)
    out = {}

    def flatten(x, name=""):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + sep)
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + sep)
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out


def load_data(data, n_jobs=1, chunk_size=1024):
    if isinstance(data, str):
        if data.endswith("jsonl.gz"):
            import gzip

            logger.info("Reading data")
            if n_jobs > 1:
                from multiprocessing import Pool

                with Pool(n_jobs) as pool:
                    data = list(pool.map(flatten_jsonl, gzip.open(data, "rt", encoding="utf-8"), chunk_size))
            else:
                data = list(map(flatten_jsonl, gzip.open(data, "rt", encoding="utf-8")))
            logger.info("Reading data done")
            return pd.DataFrame(data)
            # return json_normalize(data)
        if data.endswith("jsonl.bz2"):
            import bz2
            from pandas.io.json import json_normalize

            data = list(map(lambda line: json.loads(line.decode()), bz2.open(data, "r")))
            return json_normalize(data)


def load_fasttext(word_index, ftm, max_features=10000):
    """Load fasttext matrix for wordlist

    usage:
       embedding_matrix = load_fasttext(word_index, FastText.load_fasttext_format(fn))
    """
    all_embs = np.stack(ftm.wv.vectors)
    emb_mean, emb_std = all_embs.mean(), all_embs.std()
    embed_size = all_embs.shape[1]

    nb_words = min(max_features, len(word_index))
    embedding_matrix = np.random.normal(emb_mean, emb_std, (nb_words, embed_size))
    for word, i in word_index.items():
        if i >= max_features:
            continue
        try:
            embedding_vector = ftm.wv.get_vector(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
        except Exception as ex:
            logger.warning("Unexpected exception: %s", repr(ex))
            pass

    return embedding_matrix


def reduce_to_surface(vec):
    # Reduce dimensions
    if vec.shape[1] > 500:
        pipeline = make_pipeline(TruncatedSVD(n_components=250), Normalizer(), TSNE())
    else:
        pipeline = make_pipeline(Normalizer(), TSNE())

    return pipeline.fit_transform(vec)


class VectorMultiply:
    def __init__(self, tfidf, wmod):
        self.tfidf = tfidf
        self.wmod = wmod

    def transform(self, x, y=None):
        return x.dot(self.M)

    def fit(self, x, y):
        ivoc = {v: k for k, v in self.tfidf.vocabulary_.items()}
        self.M = np.array(
            list(map(lambda x: (self.wmod.get_word_vector(x[1])), sorted(ivoc.items(), key=lambda x: x[0])))
        )
        return self

    def fit_transform(self, x, y):
        return self.fit(x, y).transform(x)


class ColumnSelector:
    """Node for selecting columns from pandas dataframe

    #    >>> c = [1, 2]
    #    >>> df = pd.DataFrame({"content": ["hello", "No!"], 'meta': ['good', 'bad'], 'c': c})
    #    >>> ColumnSelector('content').transform(df).tolist()
    #    ['hello', 'No!']
    #    >>> np.array(ColumnSelector('content meta'.split()).transform(df)).tolist()
    #    [['hello', 'good'], ['No!', 'bad']]
    """

    def __init__(self, columns, sep=None):
        self.columns = columns
        self.sep = sep

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        assert isinstance(X, pd.DataFrame)

        try:
            if isinstance(self.columns, list):
                for col in self.columns:
                    if col not in X:
                        X[col] = ""
                if self.sep is not None:
                    return pd.Series(np.sum([X[col] + self.sep for col in self.columns], axis=0))
                return X[self.columns]
                # return pd.Series(np.sum([X[col] for col in self.columns], axis=0))
            return X[self.columns]
        except KeyError:
            cols_error = list(set(self.columns) - set(X.columns))
            raise KeyError("The DataFrame does not include the columns: %s" % cols_error)


def thin_easy_gen(X, y, basemodel, f=lambda x: x):
    y_pred = basemodel.predict_proba(X)[:, 0]
    for correct, line, res in zip((y_pred > 0.5) == y, X, y):
        if correct:
            if np.rand() < f(np.abs(y_pred - 0.5) * 2):
                continue
        yield line, res


def deep_replace_na(arr, b):
    if isinstance(arr, list):
        return [deep_replace_na(v, b) for v in arr]
    if isinstance(arr, tuple):
        return tuple(deep_replace_na(v, b) for v in arr)
    if isinstance(arr, dict):
        return {k: deep_replace_na(v, b) for k, v in arr.items()}
    try:
        if np.isnan(arr):
            return b
    except:
        pass
    return arr


def to_named_dfs(named_dfs):
    if isinstance(named_dfs, dict):
        named_dfs = list(named_dfs.items())
    ret = []
    for name, df in named_dfs:
        if isinstance(df, dict):
            df = named_df_concat(df)
        if not isinstance(df, pd.DataFrame):
            columns = list(range(df.shape[1]))
            if len(columns) == 1:
                columns = [""]
            df = pd.DataFrame(df, columns=columns)
        ret.append((name, df))
    return ret


def named_df_concat(named_dfs, index=None):
    named_dfs = to_named_dfs(named_dfs)
    names = [x[0] for x in named_dfs]
    columns = [x[1].columns for x in named_dfs]
    column_tuples = [
        tuple([name] + list([c] if isinstance(c, str) else c)) for name, x in zip(names, columns) for c in x
    ]
    column_tuples = pd.MultiIndex.from_tuples(column_tuples)
    column_tuples = deep_replace_na(list(column_tuples), "")

    if index is not None:
        df = pd.concat([x[1].set_index(index) for x in named_dfs], axis=1)
    else:
        df = pd.concat([x[1] for x in named_dfs], axis=1)
    new_columns = pd.MultiIndex.from_tuples(column_tuples)
    df.columns = new_columns

    return df


def todf(vec, *args, names=[]):
    orig_cols = []
    try:
        orig_cols = args[0].columns
    except:
        pass
    cols = vec.shape[1]
    columns = [repr(x) for x in range(cols)]
    if cols == len(orig_cols):
        columns = orig_cols
        cols = 0
    elif cols == 2:
        columns = ["x", "y"]
    if cols == 1:
        columns = names
    else:
        columns = pd.MultiIndex.from_product([names, columns])
    return pd.DataFrame(vec, columns=columns)


class ToTransformer:
    """Convert classifier to transformer"""

    def __init__(self, node):
        self.node = node

    def fit(self, X, y, **kwargs):
        self.node.fit(X, y, **kwargs)
        self

    def transform(self, X, **kwargs):
        return self.node.predict_proba(X, **kwargs)

    def fit_transform(self, X, y):
        self.fit(X, y)
        return self.transform(X)


class PandasPipeline(Pipeline):
    def transform(self, *args, **kwargs):
        vec = super().transform(*args, **kwargs)
        return todf(vec, *args)

    def fit_transform(self, *args, **kwargs):
        vec = super().fit_transform(*args, **kwargs)
        return todf(vec, *args)


class PandasUnion(FeatureUnion):
    def transform(self, *args, **kwargs):
        named_vecs = [(name, t.transform(*args, **kwargs)) for name, t in self.transformer_list]
        index = args[0].index if isinstance(args[0], pd.DataFrame) else None
        return named_df_concat(named_vecs, index=index)

    def fit_transform(self, *args, **kwargs):
        named_vecs = [(name, t.fit_transform(*args, **kwargs)) for name, t in self.transformer_list]
        index = args[0].index if isinstance(args[0], pd.DataFrame) else None
        return named_df_concat(named_vecs, index=index)


def make_pandas_union(*T):
    if len(T) == 1 and isinstance(T[0], dict):
        T = list(T[0].items())
    if isinstance(T, list):
        return PandasUnion(T)
    return PandasUnion(make_union(T).transformer_list)


def to_pandas_transformation(T):
    """Convert transformation to produce pandas dataframes

    >>> df = pd.DataFrame([{"a": 15},{"a": 1}])
    >>> from sklearn.preprocessing import StandardScaler
    >>> ss =  StandardScaler()
    >>> ss.fit_transform(df)
    array([[ 1.],
           [-1.]])
    >>> ss =  to_pandas_transformation(StandardScaler)()
    >>> ss.fit_transform(df)
      StandardScaler
                   a
    0            1.0
    1           -1.0
    >>> df = pd.DataFrame([{"a": 15, "b": 1},{"a": 1, "b": 15}])
    >>> ss =  to_pandas_transformation(StandardScaler)()
    >>> ss.fit_transform(df)
      StandardScaler ...
                   a    b
    0            1.0 -1.0
    1           -1.0  1.0
    """

    def fn(self, *args, name="predict"):
        orig_method = getattr(T, name)
        vec = orig_method(self, *args)
        return todf(vec, *args, names=[T.__name__])

    class_body = {t: partialmethod(fn, name=t) for t in ["transform", "fit_transform", "predict"]}

    NewClass = type("Pandas" + T.__name__, (T,), class_body)

    return NewClass


class ConstTransformation(BaseEstimator):
    def __init__(self, v):
        self.v = v

    def fit(self, *args, **kwargs):
        return self

    def transform(self, X, *args, **kwargs):
        return np.array([[self.v] for _ in range(len(X))])

    def fit_transform(self, *args, **kwargs):
        return self.fit(*args, **kwargs).transform(*args, **kwargs)
