import hashlib
from graphviz import Digraph


def hashstr2(s):
    hash_object = hashlib.md5(s.encode())
    ret = hash_object.hexdigest()
    while ret[0].isnumeric():
        ret = ret[1:]
    return ret[:10]


def hashstr(s, lmax=48):
    ret = s
    for i in range(0, min(len(s), lmax)):
        if not s[i].isalpha():
            ret = s[:i]
            break
    ret = ret[:lmax]
    return ret + "_" + hashstr2(s)[:4]


def flatten(steps):
    ret = []
    for s in steps:
        if isinstance(s, str):
            ret.append(s)
        else:
            ret.extend(flatten(s))
    return ret


def exclude_bad_values(v, limit=64):
    if not isinstance(v, (bool, int, str, float, list, dict)):
        return False
    if len(repr(v)) > limit:
        return False
    return True


def exclude_bad_keys(k, filterout=[]):
    return k not in filterout


def render_value(v, limit=48):
    if isinstance(v, list):
        if len(v) > 3:
            v = v[:2] + ["..."]
    ret = f"{v}"
    if len(ret) > limit:
        ret = ret[:limit] + "..."
        if ret[0] == "[":
            ret = ret + "]"
    return ret


def serialize(step):
    try:
        ret = "\n".join(
            [
                f"    {k}: " + render_value(v)
                for k, v in step.__getstate__().items()
                if exclude_bad_keys(k) and exclude_bad_values(v)
            ] + ["id: " + str(id(step))]
        )
        return [ret + "\n"] if len(ret) else []
    except AttributeError as err:
        return [f"Serialization failed\n{err}"]


class Union(list):
    def __repr__(self):
        return "Union(" + super(Union, self).__repr__() + ")"


def model_graph(model):
    ret = []
    is_union = False
    is_pipe = False
    try:
        for name, step in model.steps:
            is_pipe = True
            ret.append(model_graph(step))
    except AttributeError:
        pass
    try:
        ret.append(Union(model_graph(step) for name, step in model.transformer_list))
        is_union = True
    except AttributeError:
        pass
    if not (is_union or is_pipe):
        node = [model.__class__.__name__] + [""] + serialize(model)
        return "\n".join(node)
    return ret


def plot_model(model):
    mg = model_graph(model)
    hashmap = {s: hashstr(s) for s in flatten(mg)}

    dot = Digraph(comment="Model")
    dot.node("input", "input", style="filled", fillcolor="yellow")
    dot.node("output", "output", style="filled", fillcolor="yellow")

    for s in flatten(mg):
        dot.node(hashmap.get(s), (s.split("\nid: ")[0]+"\n").replace("\n", "\\l"), shape="box")

    def draw_edges(dot, mg, prev=["input"]):
        if isinstance(mg, str):
            s = hashmap.get(mg)
            # Add edges to all previous nodes
            for p in set(prev):
                dot.edge(p, s)
            return [s]
        ret = []
        for s in mg:
            p2 = draw_edges(dot, s, prev)
            if isinstance(mg, Union):
                # In the case of union, the prev node stays the same
                # and the next node connects to all components of the union
                ret.extend(p2)
            else:
                # In the case of pipeline, the prev node is always updated
                # and the next node is connected to the last node
                prev = p2
                ret = prev
        return ret

    last_step = draw_edges(dot, mg)
    for p in set(last_step):
        dot.edge(p, "output")
    return dot
