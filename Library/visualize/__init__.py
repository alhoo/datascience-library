import numpy as np
import pandas as pd

from itertools import chain

from bokeh.models import CustomJS, ColumnDataSource, Slider, DateRangeSlider
from bokeh.models import HoverTool
from bokeh.layouts import column, row
from bokeh.models import TableColumn, DataTable
from bokeh.plotting import figure
from bokeh.models.widgets import HTMLTemplateFormatter, Div
from bokeh.models.widgets.inputs import TextInput
from bokeh.embed import file_html
from bokeh.resources import INLINE
from bokeh.palettes import Category20

from Library.utils import named_df_concat

import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s %(name)s %(levelname)s: %(message)s", level=logging.INFO)


def create_column_datasource(pdf, plot_dfs=None):
    def unique(arr):
        ret = []
        for it in arr:
            if it not in ret:
                ret.append(it)
        return ret

    if plot_dfs is not None:
        pdf = named_df_concat([("meta", pdf), ("plot", plot_dfs)])

    figure_names = unique(pdf["plot"].columns.get_level_values(0))
    vector_space_names = unique(pdf["plot"][figure_names[0]].dimensions.columns.get_level_values(0))

    for name in figure_names:
        if "color" in pdf["plot", name]:
            continue
        N = pdf["plot", name, "label"].max() + 1
        colormappings = Category20[20]
        if N < 20:
            colormappings = Category20[N]
        if N > 20:
            continue
        pdf["plot", name, "color", "", ""] = pdf["plot", name, "label"].apply(colormappings.__getitem__)

    cd = ColumnDataSource(pdf).data
    cd["idx"] = np.arange(len(pdf))
    for fname in figure_names:
        cd[f"plot_{fname}_dimension_final_xa"] = np.array(pdf.T.loc["plot", fname, "dimensions", :, "x"].T)
        cd[f"plot_{fname}_dimension_final_ya"] = np.array(pdf.T.loc["plot", fname, "dimensions", :, "y"].T)
        cd[f"plot_{fname}_dimension_final_x"] = 50 * pdf["plot"][fname]["dimensions"][vector_space_names[0]]["x"]
        cd[f"plot_{fname}_dimension_final_y"] = 50 * pdf["plot"][fname]["dimensions"][vector_space_names[0]]["y"]
    return pdf, figure_names, vector_space_names, ColumnDataSource(cd)


source_select_js = """
    var inds = cb_obj.indices;
    var d1 = s1.data;
    var d2 = s2.data;
    var plot_names = plot_names;
    console.log(plot_names);
    for(var k in d2){ d2[k] = []; }
    for (var i = 0; i < inds.length; i++) {
        var j = inds[i];
        for(var k in d2){
            if (d1.hasOwnProperty(k)){
                d2[k].push(d1[k][j]);
            } else {d2[k].push(''); }
        }
    }
    console.log(d2)
    s2.change.emit();
    """

size_js = """
    var data = source.data;
    var plot_names = plot_names;
    var k = cb_obj.value;
    var select = false;
    for (var p = 0; p < plot_names.length; p++){
        var pname = plot_names[p];

        var x = data['plot_'+pname+'_size__'];
        var col = data['plot_'+pname+'_size__'];
        results = [];
        for (var i = 0; i < x.length; i++){
            if(x[i] < k){ col[i] = 'red'; if(select) { results.push(i); }}
            else { col[i] = 'blue'; }
        }
    }
    source.selected.indices = results;
    source.change.emit();
"""
rotate_js = """
    var data = source.data;
    var time = sliders[0].value;
    var rot = sliders[1].value;
    var t = (1 - Math.pow(1 - time,2))*Math.PI/2
    var f = sliders.slice(2).map((x) => x.value)
    function sum(a) { return a.reduce(function(a, b) { return a + b; }, 0)}
    var time = data['idx'];


    for (var p = 0; p < plot_names.length; p++){
        var pname = plot_names[p];

        var x = data['plot_'+pname+'_dimension_final_x'];
        var y = data['plot_'+pname+'_dimension_final_y'];
        var xa = data['plot_'+pname+'_dimension_final_xa'];
        var ya = data['plot_'+pname+'_dimension_final_ya'];

        for (var i = 0; i < x.length; i++) {
            x[i] = sum(f.map((v, j) => v
                        * (xa[i][j]*Math.cos(rot) - ya[i][j]*Math.sin(rot))
                        * Math.cos(t) + Math.sin(t)*time[i]));
            y[i] = sum(f.map((v, j) => v
                        * (xa[i][j]*Math.sin(rot) + ya[i][j]*Math.cos(rot))
                        ));
        }
    }
    source.change.emit();
    """


textsearch_js = """
    const d1 = s1.data;
    var drange = dr;
    var textin = ti;
    var plot_names = plot_names;
    console.log(plot_names);
    var start = drange.value[0];
    var end = drange.value[1];

    console.log(start, end);

    const results = [];
    const search_string = textin.value.toLowerCase();
    var search = new RegExp('.*'+search_string);
    for(let i = 0; i < d1['content'].length; i++){
        const row_text = (d1['content'][i]).toLowerCase();
        const time = d1['datetime'][i];
        // if (row_text.includes(search_string) && (time >= start && time <= end)){ results.push(i); }
        if (search.exec(row_text) && (time >= start && time <= end)){ results.push(i); }
    }
    s1.selected.indices = results;
    s1.change.emit();
    """


def multiscatterplot(meta_df, named_dfs=None, timecolumn="index", daterangeslider=False, maxwidth=1000):
    """
    Create a plot from df. Assume df to contain x[n], y[n] and vector_space_name[n] and have some column that could be
    used as the time dimension.

    FIXME: date range slider is combined with the text search. Rename the search function?
    FIXME: text search assumes that there is a 'content' column in the dataframe. Add content column parameter?
           Search everywhere?

    >>> from sklearn.pipeline import make_pipeline
    >>> from sklearn.feature_extraction.text import CountVectorizer
    >>> from sklearn.decomposition import TruncatedSVD
    >>> from sklearn.datasets import fetch_20newsgroups
    >>> from Library.documentspace import DocumentSpace
    >>> from sklearn.cluster import KMeans
    >>> data = fetch_20newsgroups()
    >>> df = pd.DataFrame({k: v for k,v in data.items() if len(v) > 1000})[:100]
    >>> T = [CountVectorizer(), TruncatedSVD(n_components=500), DocumentSpace()]
    >>> model = make_pipeline(*T)
    >>> xy_pca4_vec = model.fit_transform(df.data)
    >>> km = KMeans(20).fit(xy_pca4_vec)
    >>> # Construct multiplot dataframe with dimensions, labels, (optional color column) and size
    >>> df_plot = named_df_concat([("dimensions", xy_pca4_vec)])
    >>> df_plot['label'] = km.labels_
    >>> df_plot["size"] = 5
    >>> (df_plot*0 + 1)[:2]
      dimensions                                      label size
            tsne      isomap       mds       pca ...
               x    y      x    y    x    y    x    y ...
    0        1.0  1.0    1.0  1.0  1.0  1.0  1.0  1.0     1    1
    1        1.0  1.0    1.0  1.0  1.0  1.0  1.0  1.0     1    1

    >>> multiscatterplot(df, df_plot)
    Row(id='...', ...)

    :param meta_df: dataframe with all wanted metadata. Same index as named_dfs
    :param named_dfs: a list of [name, df] pairs to plot. Must have dimensions, label and size.
    :param timecolumn: column to use as the time dimension (default: index)
    :param daterangeslider: enable date range slider (default: false)
    :param maxwidth: total width to reserve for plots
    :return: document space plot
    """

    # Data source
    # if not isinstance(named_dfs, list):
    if named_dfs is not None:
        if not isinstance(named_dfs, list):
            named_dfs = [("scatterplot", named_dfs)]

        named_dfs = [(name, df.copy()) for name, df in named_dfs]

        named_dfs = named_df_concat(named_dfs)

    pdf, plot_names, vector_space_names, source = create_column_datasource(meta_df, named_dfs)

    default_slider_values = [50] + [0] * (len(vector_space_names) - 1)

    # Data table:

    column_html_template = """<p style="white-space: initial; overflow-y: scroll; height: 100%"><%= value %></p>"""
    htmltemplateformatter200 = HTMLTemplateFormatter(template=column_html_template)

    def datatable_column_filter(x):
        return "meta_" in x

    s2 = ColumnDataSource(data=dict(list(map(lambda x: (x, []), source.column_names))))
    columns = [
        TableColumn(field=name, title=name[5:].rstrip("_"))
        if name != "content"
        else TableColumn(field=name, title=name[5:].rstrip("_"), formatter=htmltemplateformatter200)
        for name in filter(datatable_column_filter, source.column_names)
    ]

    data_table = DataTable(
        source=s2,
        columns=columns,
        editable=False,
        index_position=-1,
        index_header="row index",
        fit_columns=True,
        height=250,
        row_height=50,
    )

    # Sliders

    sliders = [
        Slider(start=0.0, end=1, value=0, step=0.01, title="time"),
        Slider(start=0.0, end=2 * 3.1415926, value=0, step=0.01, title="rotate"),
    ] + [
        Slider(start=0, end=300, value=default_slider_values[idx], step=1, title=vector_space_names[idx])
        for idx in range(len(vector_space_names))
    ]

    rotate_callback = CustomJS(args=dict(source=source, sliders=sliders, plot_names=plot_names), code=rotate_js)
    source_select_callback = CustomJS(args=dict(s1=source, s2=s2, plot_names=plot_names), code=source_select_js)
    # size_callback = CustomJS(args=dict(source=source, cb_obj2=sliders[-1], plot_names=plot_names), code=size_js)

    source.selected.js_on_change("indices", source_select_callback)

    for slider in sliders:
        slider.js_on_change("value", rotate_callback)
    # sliders.append(Slider(start=0.0, end=1, value=0, step=0.01, title="color threshold"))

    # sliders[-1].js_on_change("value", size_callback)

    start = source.data[timecolumn].min()
    end = source.data[timecolumn].max()
    logger.info("Start and end: %s, %s", start, end)

    # Header

    title_div = Div(text="""<h1>Sample exploration tool</h1>""")

    toolbar = [title_div]

    if daterangeslider:
        dr = DateRangeSlider(title="Date Range: ", start=start, end=end, value=(start, end), step=1)

        ti = TextInput(placeholder="Enter search text")
        ti.callback = CustomJS(args=dict(s1=source, s2=s2, dr=dr, ti=ti, plot_names=plot_names), code=textsearch_js)

        dr.callback = CustomJS(args=dict(s1=source, s2=s2, dr=dr, ti=ti, plot_names=plot_names), code=textsearch_js)
        toolbar = toolbar + [ti, dr]

    # Plotting

    _tools_to_show = "box_zoom,lasso_select,pan,save,hover,reset,tap,wheel_zoom"
    TOOLTIPS = (
        '<table style="width: 400px; vertical-align: top">'
        + "".join(
            [
                '<tr><td style="vertical-align: top; font-weight: bold">%s</td><td>@%s</td></tr>' % (col, col)
                for col in filter(datatable_column_filter, source.column_names)
            ]
        )
        + "</table>"
    )

    l_w = max(1, min(4, int(np.sqrt(len(plot_names)))))

    p = {}
    for name in plot_names:
        p[name] = figure(
            plot_height=500, plot_width=int(maxwidth / l_w), tools=_tools_to_show, active_drag="lasso_select"
        )
        p[name].circle(
            source=source,
            x="plot_{}_dimension_final_x".format(name),
            y="plot_{}_dimension_final_y".format(name),
            color="plot_{}_color__".format(name),
            radius="plot_{}_size__".format(name),
        )
        hover = p[name].select(dict(type=HoverTool))
        hover.tooltips = TOOLTIPS

    def chunk(lst, n):
        return [lst[i : i + n] for i in range(0, len(lst), n)]  # noqa

    layout = row(
        [
            column(
                [
                    column(
                        [
                            row(
                                [
                                    column([Div(text="""<h2>{}</h2>""".format(name.replace("_", " "))), p[name]])
                                    for name in plot_name_row
                                ]
                            )
                            for plot_name_row in chunk(plot_names, l_w)
                        ],
                        sizing_mode="scale_width",
                    ),
                    data_table,
                ],
                sizing_mode="scale_width",
            ),
            column(toolbar + sliders),
        ]
    )

    return layout


def main():
    import json
    import pandas as pd
    import fileinput

    logger.info("Building dataframe")

    data = [json.loads(line) for line in fileinput.input()]
    df = pd.DataFrame(data)
    logger.info("Plotting\n%s", df)

    p = scatterplot(df, daterangeslider=False)

    logger.info("Showing %s", p)
    html = file_html(p, INLINE)
    with open("visualize.html", "w") as f:
        f.write(html)
    # print(html, "firefox")


if __name__ == "__main__":
    main()
