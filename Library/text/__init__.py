from itertools import islice, chain
from collections import OrderedDict, Counter
import re
import html

from sklearn.utils import check_array

import six

import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import hstack as sparse_hstack

from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.models import Word2Vec as gensimWord2Vec
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction.text import VectorizerMixin, CountVectorizer, _document_frequency
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from keras.preprocessing import sequence

import sentencepiece as spm
import youtokentome as yttm

import tempfile
import os

import logging

from .lm import LanguageModel  # noqa
from .artm import ArtmTransformer  # noqa

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# logger.addHandler(logging.StreamHandler())


class Transformation(BaseEstimator, TransformerMixin):
    pass


class Word2Vec(TransformerMixin):
    def __init__(self, **kwargs):
        self.kwargs = kwargs
        self.norm_vectors = None
        self.model = None

    def fit(self, X, y=None):
        self.model = gensimWord2Vec(X, **self.kwargs)
        self.norm_vectors = (self.model.wv.vectors.T / np.linalg.norm(self.model.wv.vectors, axis=1)).T
        self.vocab = {k: v.index for k, v in self.model.wv.vocab.items()}
        self.vocab["unk"] = len(self.vocab)
        self.norm_vectors = np.vstack([self.model.wv.vectors, np.zeros((1, 100))])
        return self

    def transform(self, X, y=None):
        ret = []
        for doc in X:
            widxs = np.array([self.vocab.get(word, self.vocab.get("unk")) for word in doc])
            ret.append(self.norm_vectors[widxs].mean(axis=0))
        return np.array(ret)


class Morfessor(Transformation):
    """Split sentence to tokens"""

    def __init__(self, vocabsize=20000, n_jobs=1):
        """
        Build a morphing node
        """
        self.fitted = False
        self.model = None
        self.model_bytes = b""
        self.vocabsize = vocabsize
        self.n_jobs = n_jobs

    def __getstate__(self):
        return {
            "fitted": self.fitted,
            "model_bytes": self.model_bytes,
            "vocabsize": self.vocabsize,
            "n_jobs": self.n_jobs,
        }

    def __setstate__(self, state):
        self.fitted = state.get("fitted", False)
        self.vocabsize = state.get("vocabsize", 20000)
        self.n_jobs = state.get("n_jobs", 1)
        with open("m.model", "wb") as f:
            f.write(state.get("model_bytes"))
        self.load_model()

    def load_model(self):
        model_path = "m.model"
        self.model = yttm.BPE(model=model_path)
        with open(model_path, "rb") as f:
            self.model_bytes = f.read()
        os.unlink("m.model")

    def fit(self, X, y=None):
        """
        >>> from sklearn.utils.estimator_checks import check_estimator
        >>> check_estimator(Morfessor)
        >>> m = Morfessor()
        >>> _ = m.fit(["moikka mitä sulle kuuluu?", "mulle kuuluu ihan hyvää", "no kiva"])
        >>> m.transform(["mitä kuuluu?"])
        [['▁mitä', '▁kuuluu?']]
        >>> import pickle
        >>> m_bytes = pickle.dumps(m)
        >>> m2 = pickle.loads(m_bytes)
        >>> m2.transform(["mitä kuuluu?"])
        [['▁mitä', '▁kuuluu?']]
        """
        # X = check_array(X, ensure_2d=False)
        X = check_array(X, ensure_2d=False, dtype=None, force_all_finite=False)
        logger.info("Compiling data for morph splitting")
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write("\n".join(X))
        f.close()
        logger.info("Learning morphs")
        yttm.BPE.train(data=f.name, vocab_size=self.vocabsize, model="m.model")
        self.load_model()
        os.unlink(f.name)
        logger.info("Learning successful")
        return self

    def transform(self, X, y=None):
        # X = check_array(X, ensure_2d=False)
        X = check_array(X, ensure_2d=False, dtype=None, force_all_finite=False)
        if self.model is None:
            self.load_model()
        logger.info("Splitting morphs")
        T = [self.model.encode(x, output_type=yttm.OutputType.SUBWORD) for x in X]
        logger.info("Done splitting morphs")
        return T

    def reverse_transform(self, X, y=None):
        return list(map(lambda x: ("".join(x).replace(u"\u2581", " "))[1:], X))

    def _more_tags(self):
        return {"X_types": "text"}


class Morfessor2(Transformation):
    """Split sentence to tokens"""

    def __init__(self, vocabsize=20000, n_jobs=1):
        """
        >>> from sklearn.utils.estimator_checks import check_estimator
        >>> check_estimator(Morfessor)
        """
        self.fitted = False
        self.model = None
        self.vocabsize = vocabsize
        self.n_jobs = n_jobs

    def __getstate__(self):
        return {
            "fitted": self.fitted,
            "model": [open("m.vocab").read(), open("m.model").read()],
            "vocab_size": self.vocabsize,
            "n_jobs": self.n_jobs,
        }

    def __setstate__(self, state):
        self.fitted = state.get("fitted", False)
        self.vocabsize = state.get("vocab_size", 20000)
        self.n_jobs = state.get("n_jobs", 1)
        with open("m.vocab") as f:
            f.write(state.get("model")[0])
        with open("m.model") as f:
            f.write(state.get("model")[1])
        self.load_model()

    def load_model(self):
        self.model = spm.SentencePieceProcessor()
        self.model.Load("m.model")

    def fit(self, X, y=None):
        """
        TODO: Write documentation and tests
        """
        X = check_array(X, ensure_2d=False)
        logger.info("Compiling data for morph splitting")
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write("\n".join(X))
        f.close()
        logger.info("Learning morphs")
        spm.SentencePieceTrainer.Train("--input={} --model_prefix=m --vocab_size={}".format(f.name, self.vocabsize))
        self.load_model()
        os.unlink(f.name)
        logger.info("Learning succesful")
        return self

    def transform(self, X, y=None):
        X = check_array(X, ensure_2d=False)
        if self.model is None:
            self.load_model()
        logger.info("Splitting morphs")
        T = list(map(self.model.EncodeAsPieces, X))
        logger.info("Done splitting morphs")
        return T

    def reverse_transform(self, X, y=None):
        return list(map(lambda x: ("".join(x).replace(u"\u2581", " "))[1:], X))

    def _more_tags(self):
        return {"X_types": "text"}


class DocumentTagger(Transformation):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return [TaggedDocument(doc, [i]) for i, doc in enumerate(X)]


class Doc2VecTransformer(Transformation):
    def __init__(self, vector_size=200, window=10, min_count=1, n_jobs=8, tag_documents=True):
        super(Doc2VecTransformer, self).__init__()
        self.vector_size = vector_size
        self.window = window
        self.min_count = min_count
        self.n_jobs = n_jobs
        self.model = None
        self.tag_documents = tag_documents

    def fit(self, X, y=None):
        if self.tag_documents:
            documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(X)]
        else:
            documents = X
        self.model = Doc2Vec(
            documents, vector_size=self.vector_size, window=self.window, min_count=self.min_count, workers=self.n_jobs
        )
        return self

    def transform(self, X, y=None):
        logger.info("Applying document space transformation")
        if self.tag_documents:
            documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(X)]
        else:
            documents = X
        T = np.array([self.model.infer_vector(d.words) for d in documents])
        logger.info("Done applying document space transformation")
        return T


def neurencode(text):
    """Encode text to nice format

    This encoding
    * reduces character set to Non upper ascii + 'TU'
    * produces upper and lowercase words so that casing information is kept, but lower and upper case words are
      recognized as the same
    * can handle the whole UTF-8 character set
    * is almost reversible (does not handle MiXEd case nicely (Assumes UPPER case))

    >>> neurencode('Hello ThEre mister')
    'T hello U there mister'
    """
    text = text.encode("ascii", "xmlcharrefreplace").decode("ascii")
    ret = ""
    state = ""
    for t in text:
        if t.isupper():
            if len(state):
                # ret += 'U'
                if state[0] != "U":  # repeating or mixed upper cases
                    state = "U" + state[1:]
                state += t.lower()
            else:
                state = "T " + t.lower()
            continue
        if t == " ":
            if len(state):
                ret += state
                state = ""
            ret += t
        else:
            state += t
    if len(state):
        ret += state
    return ret


def neurdecode(text):
    """Dencode text

    >>> neurdecode('T hello U there mister')
    'Hello THERE mister'
    """
    dec = ""
    skip = False
    state = 0
    for t in text:
        if skip:
            skip = False
            continue
        if t == "T":
            state = 1
            skip = True
            continue
        if t == "U":
            skip = True
            state = 2
            continue
        if t == " ":
            state = 0
        if state == 1:
            dec += t.upper()
            state = 0
        elif state == 2:
            dec += t.upper()
        else:
            dec += t
    return html.unescape(dec)


class Tokenizer:
    def __init__(self, stemmer=None):
        self.splitre = re.compile(r"[^&#;0-9a-zA-ZÄÖÅäöå]+")
        self.stemmer = stemmer

    def fit(self, X, y=None):
        return self

    def _neurstem(self, w):
        if len(w) < 2:
            return w
        return neurencode(self.stemmer.stem(neurdecode(w)))

    def transform(self, X, y=None):
        """Transform text to token lists
        >>> list(Tokenizer().transform(['Hello']))
        [['T', 'hello']]
        """
        if isinstance(X, list):
            X = pd.Series(X)
        if self.stemmer is not None:
            return X.map(lambda x: list(map(self._neurstem, self.splitre.split(neurencode(x)))))
        return X.map(lambda x: self.splitre.split(neurencode(x)))

    def fit_transform(self, X, y=None):
        return self.fit(X).transform(X)


class WordShape:
    def __init__(self, word_classes=None):
        if word_classes is None:
            word_classes = [
                ("<YEAR>", r"(19|20)[0-9]{2}"),
                ("<PERCENTAGE>", r"(100|[0-9][0-9]?)(.[0-9]+)?%"),
                ("<NUM4>", r"[0-9 ]{1,4}"),
                ("<DATE>", r"(19|20)?([0-9]{6}|[0-9]{2}([-/][0-9]{2}){1,2})"),
                ("<TIME>", r"[0-9]{2}(:[0-9]{2}){1,2}"),
                ("<NUM6>", r"[0-9 ]{1,6}"),
                ("<NUM8>", r"[0-9 ]{1,8}"),
                ("<NUM>", r"[0-9 ]+"),
                ("<HEX>", r"[0-9a-f ]*[0-9][0-9a-f ]*"),
                ("<ascii5>", r"[a-z ]{1,5}"),
                ("<ascii8>", r"[a-z ]{1,8}"),
                ("<ascii12>", r"[a-z ]{1,12}"),
                ("<ascii>", r"[a-z ]+"),
                ("<latin>", r"[a-zäöä ]+"),
                ("<weight>", r"[0-9.]+[a-z]?g"),
                ("<amount>", r"[0-9.]+[a-z]+"),
                ("<cash>", r"[0-9.]+[€$]"),
                ("<url>", r"([a-z]+://)+[a-z.:/#]+"),
                ("<email>", r"[a-zäö ]+@[a-zäö. ]+"),
                ("<alnum>", r"[0-9a-zäö ]*[0-9][0-9a-zäö]*"),
                ("<md-title>", r"# [a-zäö ]*"),
                ("<excotic1>", r"[a-zäö ]*.[a-zäö ]*"),
                ("<excotic2>", r"[a-zäö ]*(.[a-zäö ]*){2}"),
                ("<ascii>", r"[a-z]+"),
            ]
        self.word_classes = OrderedDict(list(map(lambda x: (x[0], re.compile(x[1])), word_classes)))
        self.wtypes = LabelEncoder()
        self.wshapes = LabelEncoder()
        self.onehott = OneHotEncoder()
        self.onehots = OneHotEncoder()

    def transform(self, doc):
        wtype, wshape = list(zip(*[self.shape(w) for w in doc]))
        wt = self.wtypes.transform(wtype).reshape(-1, 1)
        ws = self.wshapes.transform(wshape).reshape(-1, 1)
        return np.hstack([self.onehott.transform(wt).todense(), self.onehots.transform(ws).todense()])

    def fit_transform(self, doc):
        wtype, wshape = list(zip(*[self.shape(w) for w in doc]))
        wt = self.wtypes.fit_transform(wtype).reshape(-1, 1)
        ws = self.wshapes.fit_transform(wshape).reshape(-1, 1)
        return np.hstack([self.onehott.fit_transform(wt).todense(), self.onehots.fit_transform(ws).todense()])

    def shape(self, w):
        ret = "<UNK>"
        for key, search in self.word_classes.items():
            if search.fullmatch(w.lower()):
                ret = key
                break
        shape = "<mixed>"
        if w.lower() == w:
            shape = "<lower>"
        elif w.title() == w:
            shape = "<title>"
        elif w.upper() == w:
            shape = "<upper>"
        return ret, shape


class SequenceVectorizer:
    def __init__(self, max_length=500, num_words=50000, unk_classes=None):
        self.word_index = {}
        self.max_length = max_length
        self.num_words = num_words
        self.unk_classes = WordShape(unk_classes)

    def fit(self, X, y=None):
        word_counts = Counter(chain(*list(X)))
        wcounts = list(word_counts.items())
        wcounts.sort(key=lambda x: x[1], reverse=True)
        unkcount = len(self.unk_classes.word_classes) + 1
        sorted_voc = ["<UNK>"] + list(self.unk_classes.word_classes.keys())
        sorted_voc.extend(wc[0] for wc in wcounts)
        self.word_index = dict(
            list(islice(zip(sorted_voc, list(range(0, len(sorted_voc) + unkcount))), self.num_words))
        )
        return self

    def _get_word(self, w):
        if w in self.word_index and self.word_index.get(w) < self.num_words:
            return self.word_index.get(w)
        return self.word_index.get(self.unk_classes.transform(w)[0])

    def transform(self, X, y=None):
        """Transfrom token lists to vectorized sequences
        >>> from sklearn.pipeline import make_pipeline
        >>> docs = ['Hello']
        >>> pl = make_pipeline(Tokenizer(), SequenceVectorizer(4))
        >>> _ = pl.fit(docs)
        >>> pl.transform(docs).tolist()
        [[0, 0, 24, 25]]
        """
        X = [list([self._get_word(x) for x in doc]) for doc in X]
        return sequence.pad_sequences(X, maxlen=self.max_length)


class ListCountVectorizerBase(CountVectorizer):
    def __init__(
        self,
        input="content",
        encoding="utf-8",
        decode_error="strict",
        strip_accents=None,
        lowercase=True,
        preprocessor=None,
        tokenizer=None,
        stop_words=None,
        token_pattern=r"(?u)\b\w\w+\b",
        ngram_range=(1, 1),
        analyzer="word",
        max_df=1.0,
        min_df=1,
        max_features=None,
        vocabulary=None,
        binary=False,
        dtype=np.int64,
    ):
        """Transfrom token list to ngram bag
        >>> from sklearn.pipeline import make_pipeline
        >>> docs = ['Hello', 'Hi']
        >>> pl = make_pipeline(Tokenizer(), ListCountVectorizerBase(min_df=0, max_df=1.0))
        >>> pl.fit_transform(docs).todense().tolist()
        [[1, 1, 0], [1, 0, 1]]
        >>> list(pl.steps[-1][1].vocabulary_.keys())[-3:]
        ['T', 'hello', 'hi']
        """
        super(ListCountVectorizerBase, self).__init__(
            input=input,
            encoding=encoding,
            decode_error=decode_error,
            strip_accents=strip_accents,
            lowercase=lowercase,
            preprocessor=preprocessor,
            tokenizer=tokenizer,
            analyzer=analyzer,
            stop_words=stop_words,
            token_pattern=token_pattern,
            ngram_range=ngram_range,
            max_df=max_df,
            min_df=min_df,
            max_features=max_features,
            vocabulary=vocabulary,
            binary=binary,
            dtype=dtype,
        )

    @staticmethod
    def build_preprocessor():
        return lambda doc: doc

    @staticmethod
    def build_tokenizer():
        return lambda doc: doc

    @staticmethod
    def decode(doc):
        return doc


class ListCountVectorizer(ListCountVectorizerBase):
    def __init__(
        self,
        input="content",
        encoding="utf-8",
        decode_error="strict",
        strip_accents=None,
        lowercase=True,
        preprocessor=None,
        tokenizer=None,
        stop_words=None,
        token_pattern=r"(?u)\b\w\w+\b",
        ngram_range=(1, 1),
        analyzer="word",
        max_df=1.0,
        min_df=1,
        max_features=None,
        vocabulary=None,
        binary=False,
        dtype=np.int64,
        unk_classes=[
            ("<NUM4>", r"^[0-9 ]{1,4}$"),
            ("<NUM6>", r"^[0-9 ]{1,6}$"),
            ("<NUM8>", r"^[0-9 ]{1,8}$"),
            ("<NUM>", r"^[0-9 ]+$"),
            ("<HEX>", r"^[0-9a-f ]*[0-9][0-9a-f]*$"),
            ("<ascii5>", r"^[a-z ]{1,5}$"),
            ("<ascii8>", r"^[a-z ]{1,8}$"),
            ("<ascii12>", r"^[a-z ]{1,12}$"),
            ("<ascii>", r"^[a-z ]+$"),
            ("<UNK>", r"^.*$"),
        ],
    ):
        """Transfrom token list to ngram bag
        >>> from sklearn.pipeline import make_pipeline
        >>> corpus = [
        ...     'This is the first document.',
        ...     'This document is the second document.',
        ...     'And this is the third one.',
        ...     'Is this the first document?',
        ... ]
        >>> tok = Tokenizer()
        >>> X = tok.fit_transform(corpus)
        >>> lcv = ListCountVectorizer(min_df=2, max_df=1.0)
        >>> pl = make_pipeline(tok, lcv)
        >>> X = pl.fit_transform(corpus)
        >>> print(X.astype(int).toarray())  # doctest: +NORMALIZE_WHITESPACE
        [[1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1]
         [1 0 0 0 0 0 0 0 0 1 0 1 2 0 1 1 1]
         [1 0 0 0 0 0 0 0 3 0 0 1 0 0 1 1 1]
         [1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1]]
        >>> len(list(pl.steps[-1][1].vocabulary_.items()))
        17
        >>> len(pl.steps[-1][1].inverse_transform(X))
        4
        """
        super(ListCountVectorizer, self).__init__(
            input=input,
            encoding=encoding,
            decode_error=decode_error,
            strip_accents=strip_accents,
            lowercase=lowercase,
            preprocessor=preprocessor,
            tokenizer=tokenizer,
            analyzer=analyzer,
            stop_words=stop_words,
            token_pattern=token_pattern,
            ngram_range=ngram_range,
            max_df=max_df,
            min_df=min_df,
            max_features=max_features,
            vocabulary=vocabulary,
            binary=binary,
            dtype=dtype,
        )
        self.unk_classes = OrderedDict(list(map(lambda x: (x[0], re.compile(x[1])), unk_classes)))

    def _limit_features(self, X, vocabulary, high=None, low=None, limit=None):
        """Remove too rare or too common features.
        Prune features that are non zero in more samples than high or less
        documents than low, modifying the vocabulary, and restricting it to
        at most the limit most frequent.
        This does not prune samples with zero features.
        """
        if high is None and low is None and limit is None:
            return X, set()

        # Calculate a mask based on document frequencies
        dfs = _document_frequency(X)
        tfs = np.asarray(X.sum(axis=0)).ravel()
        mask = np.ones(len(dfs), dtype=bool)
        if high is not None:
            mask &= dfs <= high
        if low is not None:
            mask &= dfs >= low
        if limit is not None and mask.sum() > limit:
            mask_inds = (-tfs[mask]).argsort()[:limit]
            new_mask = np.zeros(len(dfs), dtype=bool)
            new_mask[np.where(mask)[0][mask_inds]] = True
            mask = new_mask

        new_indices = np.cumsum(mask) - 1 + len(self.unk_classes)  # maps old indices to new
        removed_terms = set()
        removed_classes = [[] for _ in self.unk_classes.keys()]
        for term, old_index in list(six.iteritems(vocabulary)):
            if mask[old_index]:
                vocabulary[term] = new_indices[old_index]
            else:
                del vocabulary[term]
                removed_terms.add(term)
                for idx, search in enumerate(self.unk_classes.values()):
                    if search.match(term):
                        removed_classes[idx].append(old_index)
                        break
        for idx, cid in enumerate(self.unk_classes.keys()):
            vocabulary[cid] = idx

        kept_indices = np.where(mask)[0]
        # X = hstack([X[:, cols].sum(axis=1) for cols in removed_classes] + [X[:, kept_indices]])
        if len(kept_indices) == 0:
            raise ValueError("After pruning, no terms remain. Try a lower" " min_df or a higher max_df.")
        X_out = sparse_hstack([X[:, cols].sum(axis=1) for cols in removed_classes] + [X[:, kept_indices]])
        # return X[:, kept_indices], removed_terms
        # return X, removed_terms
        if type(X_out) != "scipy.sparse.csr.csr_matrix":
            logger.warn("Type of X_out is %s. We want csr matrix.", type(X_out))
            X_out = X_out.tocsr()
            logger.warn("After converting type of X_out: %s", type(X_out))
        X_out.indices
        return (X_out, removed_terms)


class OldListCountVectorizer(VectorizerMixin):
    def __init__(
        self,
        min_df=2,
        max_df=0.4,
        ngram_range=[1, 1],
        unk_classes=[
            ("<NUM4>", r"[0-9 ]{1,4}"),
            ("<NUM6>", r"[0-9 ]{1,6}"),
            ("<NUM8>", r"[0-9 ]{1,8}"),
            ("<NUM>", r"[0-9 ]+"),
            ("<HEX>", r"[0-9a-f ]*[0-9][0-9a-f]*"),
            ("<ascii5>", r"[a-z ]{1,5}"),
            ("<ascii8>", r"[a-z ]{1,8}"),
            ("<ascii12>", r"[a-z ]{1,12}"),
            ("<ascii>", r"[a-z ]+"),
        ],
    ):
        self.word_index = {}
        self.ngram_range = ngram_range
        # self.num_words = num_words
        self.min_df = min_df
        self.max_df = max_df
        self.unk_classes = OrderedDict(list(map(lambda x: (x[0], re.compile(x[1])), unk_classes)))
        self.stop_words = None

    def fit(self, X, y=None):
        doc_count = len(X)

        unkcount = len(self.unk_classes) + 1
        sorted_voc = ["<UNK>"] + list(self.unk_classes.keys())

        word_counts = Counter(chain(*list(X)))
        wcounts = list(word_counts.items())
        wcounts.sort(key=lambda x: x[1], reverse=True)

        def not_common_or_rare(x):
            return self.min_df <= x[1] <= self.max_df * doc_count

        sorted_voc.extend(wc[0] for wc in filter(not_common_or_rare, wcounts))
        self.word_index = dict(list(zip(sorted_voc, list(range(0, len(sorted_voc) + unkcount)))))
        return self

    def _get_word(self, w):
        if w in self.word_index:  # and self.word_index.get(w) < self.num_words:
            return self.word_index.get(w)
        for key, search in self.unk_classes.items():
            if search.match(w):
                return self.word_index[key]
        return self.word_index["<UNK>"]

    def transform(self, X, y=None):
        """Transfrom token list to ngram bag
        >>> from sklearn.pipeline import make_pipeline
        >>> docs = ['Hello']
        >>> pl = make_pipeline(Tokenizer(), OldListCountVectorizer(min_df=0, max_df=1))
        >>> _ = pl.fit(docs)
        >>> pl.transform(docs).todense().tolist()
        [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1]]
        >>> list(pl.steps[-1][1].word_index.keys())[-2:]
        ['T', 'hello']
        """
        j_indices = []
        indptr = [0]
        values = []
        for doc in X:
            feature_counter = Counter((self._get_word(x) for x in self._word_ngrams(doc, self.stop_words)))
            j_indices.extend(feature_counter.keys())
            values.extend(feature_counter.values())
            indptr.append(len(j_indices))
        X = csr_matrix((values, j_indices, indptr), shape=(len(indptr) - 1, len(self.word_index)), dtype=int)
        return X


if __name__ == "__main__":
    import doctest

    doctest.testmod()
