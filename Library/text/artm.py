from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction.text import CountVectorizer

try:
    import artm
except ImportError:
    print(
        """Failed to import artm. [Install artm](https://bigartm.readthedocs.io/en/stable/installation/linux.html):

    # Step 3. Clone repository and build
    git clone --branch=stable https://github.com/bigartm/bigartm.git
    cd bigartm
    mkdir build && cd build
    cmake ..
    make

    # Step 4. Install BigARTM
    make install
    export ARTM_SHARED_LIBRARY=/usr/local/lib/libartm.so

    # Alternative step 4 - installing only the Python package
    pip install python/bigartm*36*.whl
    """
    )
    raise


class ArtmTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, num_components=20, tokenizer=CountVectorizer(), num_collection_passes=10, vocab=None):
        self.num_components = num_components
        self.num_collection_passes = num_collection_passes
        self.tokenizer = tokenizer
        self.tfitted = False
        if vocab is not None:
            self.tfitted = True
            self.full_vocabulary = vocab

    def tokenize(self, X):
        if self.tfitted:
            tok = self.tokenizer.transform(X)
        elif self.tokenizer is not None:
            tok = self.tokenizer.fit_transform(X)
            try:
                self.full_vocabulary = {}
                pos = 0
                for tname, t in self.tokenizer.transformer_list:
                    for k, v in t.vocabulary_.items():
                        self.full_vocabulary[v + pos] = k
                    pos += len(t.vocabulary_)
            except:  # noqa
                self.full_vocabulary = {v: k for k, v in self.tokenizer.vocabulary_.items()}
            finally:
                self.tfitted = True
        else:
            # No tokenizer but the tokenizer is fitted. Assuming we get tokens as input
            tok = X
        return artm.BatchVectorizer(data_format="bow_n_wd", n_wd=tok.T, vocabulary=self.full_vocabulary)

    def fit(self, X, y=None):
        batch_vectorizer = self.tokenize(X)
        self.model = artm.ARTM(num_topics=self.num_components, dictionary=batch_vectorizer.dictionary)
        self.model.scores.add(
            artm.PerplexityScore(name="my_first_perplexity_score", dictionary=batch_vectorizer.dictionary)
        )
        self.model.fit_offline(batch_vectorizer=batch_vectorizer, num_collection_passes=self.num_collection_passes)
        return self

    def transform(self, X):
        batch_vectorizer = self.tokenize(X)
        theta_test = self.model.transform(batch_vectorizer=batch_vectorizer)
        return theta_test
