from . import varikn

import os
import re
import tempfile
import numpy as np

from sklearn.base import BaseEstimator
from sklearn.utils import check_array

from arpa.parsers.quick import ARPAParserQuick
from arpa.models.simple import ARPAModelSimple

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class LanguageModel(BaseEstimator):
    dscale1 = 1
    dscale2 = 0
    cutoffs = (0, 0, 1)
    hashsize = 100
    ndrop = 0
    nfirst = -1
    space_norm = re.compile(r"\s+")

    def __init__(self, input_splitted=False):
        self.input_splitted = input_splitted

    def normalize_sentence(self, sent):
        if self.input_splitted:
            ret = self.space_norm.sub(" ", (" ".join(sent)).lower())
        else:
            ret = self.space_norm.sub(" ", sent.lower())
        if len(ret) == 0:
            ret = " "
        return "<s> " + ret + " </s>"

    def fit(self, X, y=None):
        """
        >>> from sklearn.utils.estimator_checks import check_estimator
        >>> check_estimator(LanguageModel)
        >>> lm = LanguageModel()
        >>> lm.fit(['a b c', 'a b d']).predict(['a b c'])
        array([-0.74766633])
        """

        if not self.input_splitted:
            X = check_array(X, ensure_2d=False)

        # Prepare temporary files
        corpusfile = tempfile.NamedTemporaryFile("w+", delete=False)
        for sent_str in X:
            corpusfile.write(self.normalize_sentence(sent_str))
        corpusfile.close()

        arpafile = tempfile.NamedTemporaryFile("w+", delete=False)
        arpafile.close()

        # Calculate the arpa file
        vgtrainer = varikn.VarigramTrainer(False, False)
        vgtrainer.set_datacost_scale(self.dscale1)
        vgtrainer.initialize(corpusfile.name, self.hashsize, self.ndrop, self.nfirst, "", "<s>", False, "")
        vgtrainer.set_cutoffs(self.cutoffs)
        vgtrainer.grow(True)
        vgtrainer.write_file(arpafile.name, True)

        # Read the arpa model
        lmreader = ARPAParserQuick(ARPAModelSimple)
        lmreader.re_entry = re.compile(
            "^(-?\\d+(\\.\\d+)?([eE]-?\\d+)?)" "\s" "((\\S+ )*?\\S+)" "([\t ](-?\\d+(\\.\\d+)?)([eE]-?\\d+)?)?$"  # noqa
        )
        self.lm = lmreader.parse(open(arpafile.name))[0]
        self.lm._unk = "<UNK>"

        # Cleanup
        os.remove(corpusfile.name)
        os.remove(arpafile.name)
        return self

    @staticmethod
    def powermean(x, k=2):
        if k == 1:
            return np.mean(x)
        return -sum(np.power(np.abs(x), k)) / sum(np.power(np.abs(x), k - 1))

    def wlogp(self, X, sos="<s>", eos=None):
        if not self.input_splitted:
            X = check_array(X, ensure_2d=False)
        words = self.lm._check_input(self.normalize_sentence(X))
        if self.lm._unk:
            words = self.lm._replace_unks(words)
        if sos:
            words = (sos,) + words
        if eos:
            words = words + (eos,)
        return np.array(list(self.lm.log_p_raw(words[:i]) for i in range(1, len(words) + 1)))

    def logp(self, X, sos="<s>", eos=None, k=2):
        return self.powermean(self.wlogp(X, sos="<s>", eos=None), k)

    def transform_one(self, X):
        wprobs = self.wlogp(X)
        return (
            np.max(wprobs),
            self.powermean(wprobs, 2),
            self.powermean(wprobs, 1),
            self.powermean(wprobs, 0),
            np.min(wprobs),
        )

    def transform(self, X):
        return np.array([self.transform_one(x) for x in X])

    def predict(self, X, y=None):
        if not self.input_splitted:
            X = check_array(X, ensure_2d=False)
        return np.array([self.lm.log_s(self.normalize_sentence(string), eos=None) / (1 + len(string)) for string in X])

    def _more_tags(self):
        return {"X_types": "text"}
