#!/usr/bin/env python

from os import path, makedirs, environ
import glob
import pathlib
import shutil

from codecs import open
import subprocess

from setuptools.command.build_ext import build_ext
from setuptools import setup, Extension
import logging

logname = "/tmp/setup.log"
logging.basicConfig(
    filename=logname,
    filemode="w",
    format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    level=logging.DEBUG,
)
logger = logging.getLogger("setup")

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()


class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=""):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            subprocess.check_output(["cmake", "--version"])
        except OSError:
            raise RuntimeError(
                "CMake must be installed to build the following extensions: "
                + ", ".join(e.name for e in self.extensions)
            )

        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        # extdir = path.abspath(path.dirname(self.get_ext_fullpath(ext.name)))
        extdir = pathlib.Path(self.get_ext_fullpath(ext.name))
        output = extdir.parent.parent.absolute() / "Library" / "text"

        extdir.mkdir(parents=True, exist_ok=True)
        output.mkdir(parents=True, exist_ok=True)
        subprocess.check_call(["git", "submodule", "update", "--init"])

        logger.info("Varikn output: %s", output)
        logger.info("Build temp dir: %s", self.build_temp)

        config = "Debug" if self.debug else "Release"

        cmake_args = [
            "-DCMAKE_BUILD_TYPE=" + config,
            "-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=" + str(output.absolute()),
            "-DPYTHON=1",
        ]
        build_args = ["--config", config, "--", "-j4"]

        env = environ.copy()

        if not path.exists(self.build_temp):
            makedirs(self.build_temp)

        subprocess.check_call(["cmake", ext.sourcedir] + cmake_args, cwd=self.build_temp, env=env)
        if not self.dry_run:
            subprocess.check_call(["cmake", "--build", "."] + build_args, cwd=self.build_temp)

            logger.info("Copying varikn to %s", output.absolute())

            for fn in glob.glob(str(self.build_temp) + "/lib/**", recursive=True):
                logger.info(fn)
                print(fn)
            variknlib = "{}/lib/python/varikn.py".format(str(self.build_temp))
            variknout = str(output.absolute()) + "/varikn.py"
            variknout2 = here + "/varikn.py"

            # subprocess.run(
            #    ("cp {} {}/".format(variknlib, str(output.absolute()))).split(), cwd=self.build_temp
            # )
            assert path.exists(variknlib)
            shutil.copyfile(variknlib, variknout)
            shutil.copyfile(variknlib, variknout2)
            shutil.copy(str(output.absolute()) + "/_varikn.so", here + "/_varikn.so")
            assert path.exists(variknout)
            assert path.exists(variknout2)

            try:
                subprocess.check_call(("python3 -m black -l120 {}".format(variknout)).split(), cwd=self.build_temp)
                subprocess.check_call(("python3 -m black -l120 {}".format(variknout2)).split(), cwd=self.build_temp)
            except:  # noqa
                pass

        shutil.rmtree(self.build_temp)
        shutil.rmtree(here + "/variKN")


setup(
    name="Library",
    version="0.6.5",
    description="Library extension for sklearn modeling",
    long_description=long_description,
    author="Lasse Hyyrynen",
    author_email="lasse.hyyrynen@pm.me",
    maintainer="Lasse Hyyrynen",
    maintainer_email="lasse.hyyrynen@pm.me",
    license="MIT",
    keywords=["sklearn"],
    download_url="https://github.com/alhoo/Library/archive/0.6.5.tar.gz",
    url="https://github.com/alhoo/Library",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Topic :: Utilities",
    ],
    setup_requires=["setuptools>=20.2.2"],
    packages=[
        "Library",
        "Library.documentspace",
        "Library.evaluate",
        "Library.input",
        "Library.postprocess",
        "Library.skeras",
        "Library.system",
        "Library.text",
        "Library.utils",
        "Library.visualize",
    ],
    ext_modules=[CMakeExtension("varikn", "variKN")],
    ext_package="variKN",
    cmdclass=dict(build_ext=CMakeBuild),
    install_requires=[
        "pandas>=0.18.0",
        "bokeh",
        "gensim",
        "arpa",
        "numpy>=1.16.0,<1.19.0",
        "scipy>=1.4.1",
        "youtokentome>=1.0.6",
        "bigartm>=0.9.2",
        "sentencepiece",
        "tensorflow-gpu>=2.1.0",
        "umap-learn==0.4.6",
        "six>=1.12.0",
        "keras>=2.3.0",
        "nltk>=1.0.0",
        "graphviz>=0.12",
        "setuptools>=41.0.0",
        "scikit-learn>=0.1.0",
    ],
    extras_require={
        "tests": [
            "pytest>=0.0.1",
            "pandas>=0.18.0",
            "pylint>=1.8.2",
            "coverage",
            "pytest",
            "pytest-flake8",
            "pytest-black ; python_version >= '3.6'",
            "pytest-cov",
        ]
    },
    test_suite="pytest",
    zip_safe=True,
)
