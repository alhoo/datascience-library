test: Library/text/varikn.py
	python3 -m pytest --doctest-modules


Library/text/varikn.py:
	git submodule update --init
	cd variKN && \
		cmake . -DCMAKE_BUILD_TYPE=Release -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=../../Library/text/ -DPYTHON=1 && \
		cmake --build . --config Release -- -j4 && \
		python3 -m black -l120 lib/python/varikn.py && \
		cp lib/python/varikn.py ../Library/text/
	rm -Rv variKN

clean:
	rm -Rf variKN Library/text/varikn.py Library/text/_varikn.so
